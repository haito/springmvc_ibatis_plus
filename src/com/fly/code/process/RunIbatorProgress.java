package com.fly.code.process;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.ibator.api.Ibator;
import org.apache.ibatis.ibator.config.IbatorConfiguration;
import org.apache.ibatis.ibator.config.xml.IbatorConfigurationParser;
import org.apache.ibatis.ibator.internal.DefaultShellCallback;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;

import com.fly.code.CodeMaker;
import com.fly.code.util.StringUtil;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;

public class RunIbatorProgress implements IRunnableWithProgress
{
    Logger log = LoggerFactory.getLogger(getClass());
    
    private static Configuration config;
    
    static
    {
        config = new Configuration();
        config.setDefaultEncoding("UTF-8");
        config.setDateTimeFormat("yyyy-MM-dd HH:mm:ss");
        config.setDateFormat("yyyy-MM-dd");
        config.setTimeFormat("HH:mm:ss");
        config.setNumberFormat("#0.#");
        config.setObjectWrapper(new DefaultObjectWrapper());
        config.setClassForTemplateLoading(RunIbatorProgress.class, "/template/springIbatis");
    }
    
    private String driver;
    
    private String dburl;
    
    private String username;
    
    private String password;
    
    private String packName;
    
    private String projectDir;
    
    private String projectSrcDir;
    
    private String[] tabName;
    
    private String prefix;
    
    // FreeMarker model
    Map<String, Object> model = new HashMap<String, Object>();
    
    public RunIbatorProgress(String driver, String dburl, String username, String password, String packName, String projectDir, String[] tabName, String prefix, String codeType)
    {
        super();
        this.driver = driver;
        this.dburl = dburl;
        this.username = username;
        this.password = password;
        this.packName = packName;
        this.projectDir = projectDir;
        this.projectSrcDir = projectDir + "src\\";
        this.tabName = tabName;
        new File(projectSrcDir).mkdirs();
        this.prefix = prefix;
    }
    
    @Override
    public void run(IProgressMonitor monitor)
        throws InvocationTargetException, InterruptedException
    {
        // 在当前目录，创建并运行脚本
        try
        {
            monitor.beginTask("生成代码", IProgressMonitor.UNKNOWN);
            monitor.subTask("自动生成DAO、Map、MODEL 代码中......");
            creatAndRunIbator(driver, dburl, username, password, packName, projectSrcDir, tabName);
            monitor.done();
        }
        catch (Exception e)
        {
            throw new InvocationTargetException(e.getCause(), e.getMessage());
        }
    }
    
    // 在当前目录，创建ibator 配置文件
    private String creatIbatorConig()
    {
        model.put("driver", driver);
        model.put("dburl", dburl);
        model.put("username", username);
        model.put("password", password);
        model.put("packName", packName);
        model.put("packagePath", packName.replace(".", "\\"));
        model.put("projectSrcDir", projectSrcDir);
        model.put("tables", tabName);
        model.put("prefix", StringUtils.trimToEmpty(prefix));
        model.put("date", new Date());
        return FreeMarkers.renderIbatorConfigTemplate(model);
    }
    
    // 运行 Ibator代码创建程序
    private void creatAndRunIbator(String driver, String dburl, String username, String password, String packName, String projectSrcDir, String[] tabName)
        throws Exception
    {
        // 直接调用ibator IbatorRunner main
        java.util.List<String> warnings = new ArrayList<String>();
        Set<String> contexts = new HashSet<String>();
        Set<String> fullyqualifiedTables = new HashSet<String>();
        StringReader configStr = new StringReader(creatIbatorConig());
        InputSource inputSource = new InputSource(configStr);
        IbatorConfigurationParser cp = new IbatorConfigurationParser(warnings);
        IbatorConfiguration config = cp.parseIbatorConfiguration(inputSource);
        DefaultShellCallback callback = new DefaultShellCallback(true);
        Ibator ibator = new Ibator(config, callback, warnings);
        ibator.generate(null, contexts, fullyqualifiedTables);
        creatTemplateCode(projectDir, packName, tabName);
    }
    
    /**
     * 生成模板代码
     * 
     * @param projectDir
     * @param packName
     * @param tabNames
     * @throws IOException
     * @see [类、类#方法、类#成员]
     */
    private void creatTemplateCode(String projectDir, String packName, String[] tabNames)
        throws IOException
    {
        for (String tableName : tabNames)
        {
            String className = StringUtil.camelCase(tableName, true);
            model.put("className", className);
            
            // 遍历map xml文件
            String fatherDir = projectDir + "src\\" + packName.replace(".", "\\");
            String mapper = fatherDir + "\\map\\";
            Set<String> mappers = new HashSet<String>();
            for (File file : new File(mapper).listFiles())
            {
                mappers.add(packName.replace(".", "\\") + "\\map\\" + file.getName());
            }
            model.put("mappers", mappers);
            URL url = CodeMaker.class.getProtectionDomain().getCodeSource().getLocation();
            log.info("File Path: {}", url.getPath());
            if (url.getPath().endsWith(".jar")) // 可运行jar内文件处理
            {
                JarFile jarFile = new JarFile(url.getFile());
                Enumeration<JarEntry> entrys = jarFile.entries();
                while (entrys.hasMoreElements())
                {
                    JarEntry jar = (JarEntry)entrys.nextElement();
                    String name = jar.getName();
                    if (!jar.isDirectory() && name.startsWith("template/springIbatis"))
                    {
                        String path = FreeMarkers.renderString(name, model);
                        String realPath = projectDir + StringUtils.substringAfter(path, "template/springIbatis");
                        if (name.endsWith(".ftl"))// 模板文件
                        {
                            realPath = realPath.substring(0, realPath.length() - 4);
                            String ftl = StringUtils.substringAfter(name, "template/springIbatis/");
                            Template template = config.getTemplate(ftl);
                            String content = FreeMarkers.renderTemplate(template, model);
                            FileUtils.writeStringToFile(new File(realPath), content, "UTF-8");
                        }
                        else
                        {
                            InputStream inputStream = CodeMaker.class.getResourceAsStream("/" + name);
                            FileUtils.copyInputStreamToFile(inputStream, new File(realPath));
                        }
                    }
                }
                jarFile.close();
            }
            else
            {
            File tFile = new File(url.getFile() + "/template/springIbatis");
            Collection<File> listFiles = FileUtils.listFiles(tFile, new String[] {"jar", "ftl", "jsp", "html", "htm", "js", "css", "map", "eot", "svg", "ttf", "woff", "woff2"}, true);
            for (File file : listFiles)
                {
                    String path = FreeMarkers.renderString(file.getAbsolutePath(), model);
                    String realPath = projectDir + StringUtils.substringAfter(path, "\\template\\springIbatis\\");
                    if (realPath.endsWith(".ftl"))
                    {
                        realPath = realPath.substring(0, realPath.length() - 4);
                        String ftl = StringUtils.substringAfter(file.getAbsolutePath(), "\\template\\springIbatis\\");
                        Template template = config.getTemplate(ftl);
                        String content = FreeMarkers.renderTemplate(template, model);
                        FileUtils.writeStringToFile(new File(realPath), content, "UTF-8");
                    }
                    else
                    {
                        FileUtils.copyFile(file, new File(realPath));
                    }
                }
            }
        }
    }
}